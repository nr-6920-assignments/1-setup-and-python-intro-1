# Turn in your homework

You'll turn in homework by *pushing* it to your assignment repository on gitlab.com, so you'll follow this process each week.

> Tip: You can push files as often as you want, and I'll just look at the last ones that were uploaded before the due date. You can also push different parts of the homework assignment at different times-- you don't have to do it all at once. Since the homework assignments are usually several scripts, a lot of students choose to push each one as they finish it.

Before you walk through how to do this, it's useful to know why you need to do the various steps. Git is used for *versioning* code (among other things, but this is the use you're interested in). You can save (or *commit*) your code, and it's like taking a snapshot of your code at that point in time. You can see what changes you made since the last snapshot, and you can revert to a snapshot if you'd like. It's kind of like backups, but much more flexible. 

Once you've committed something, it's there forever. Even if you delete it, git still remembers that file so that you can get it back if you need to. So be very careful what you commit. If you commit a huge GIS dataset, then it'll upload it with your homework, which will take forever. Also, please only commit and turn in the files that I've asked for. Wading through a million files to find the correct ones is not my idea of fun and will make me grumpy.

Alrighty, now let's see how to do this.

1\. Open GitHub Desktop. It'll probably load the last repository you had open, which may or may not be the one you want to use. Check the upper left corner to see which repository it has open, and use the dropdown list to select the assignment you want to turn in.

![wrong repo](images/github-repo.png)

2\. Once your homework repository is loaded, you'll see a list of the files in your homework folder that have changed since the last time you committed anything. It's showing me the three notebooks from this assignment.

![dirty](images/github-changed-files.png)

3\. Make sure that the checkboxes next to the files you want to turn in are checked (and that any you don't want to turn in aren't checked). This is called *staging* the files, and tells GitHub Desktop which files you want it to save. *Make sure you don't accidentally highlight any lines in the file, because if you do, they're the only ones that will be committed and turned in.* 

![staged lines](images/github-staged-lines.png)

4\. Add a short message in the Summary box. Make the message something meaningful. Specifying the homework assignment is good, but you can more detailed if you want-- just keep it short. If you do want to add something longer, use the Description box.

When your assignment was created, an *issue* with a due date was automatically created (you should have gotten an email about it that included the URL for cloning the assignment). If this issue is still open then GitLab will send you nag messages (if this gets to be too annoying I can get rid of the due dates, but I thought it might be useful to see them when logged into GitLab). You can *close* the issue to stop being nagged. To do that, add `Closes #1` to the message  or description for your final assignment commit (you can use just that text with nothing else, if you'd like). If you forget to do it this way, you can log into GitLab, go to the assignment, choose **Issues** from the menu on the left, click on the assignment issue, and then hit the **Close issue** button in the upper right corner.

![commit](images/github-commit.png)

5\. Hit the Commit to master button. **This saves your snapshot but does not upload any files yet.** All of the file information for the checked files will disappear. This means that they don't have changes since the last commit (which you just did).

6\. Now there'll be an option to *push* your committed changes to GitLab. The number 1 next to an *up* arrow means that there is one commit that hasn't been pushed yet. Go ahead and hit the Push origin button.

![gitlab](images/github-push.png)

7\. After you do this, you can log in to your GitLab account and look in your assignment project to verify that the modified files are there. You'll see the message that you used when committing your files, and if you click on a filename it'll display the notebook in HTML format so you can verify that it has your changes. **If you're concerned that the correct files may not have been uploaded, please log into GitLab and look for yourself rather than asking me if they're there.** You and I can see the exact same files, and you can log in and check just as easily as I can.

![gitlab](images/gitlab-files.png)

8\. You might have noticed the **Changes** and **History** tabs in GitHub Desktop. The Changes view is the default one, and shows you all of the file changes that haven't been committed yet. That's the view you need to use when turning in your homework. The History tab shows a list of commits. If you take a look at that, you'll see the commit you just did and also all of the commits I've made to the original repository. If you click on one, you'll see a list of files that were changed with that commit, and if you click on a file then you'll see the exact changes that were made (red lines show the old version, and green is the new). It's a pretty nifty way of remembering what you did and when!

![gitlab](images/github-history.png)

9\. You will get an email with your personalized URL when I post the next assignment.
