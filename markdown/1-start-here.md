# Intro to Python

## Why learn to write code?

- From what I hear, most GIS jobs want scripting experience these days.
- Scripting will help you in a lot of jobs, GIS-related or not.
- I think you better understand what you're doing if you're writing code instead of pointing and clicking.
- You can automate boring and repetitive tasks (yay!).
- Impress your friends and coworkers.

### Some of my personal reasons

- I hate busywork. Doing the same thing over and over bores me to death, so I'd much rather automate my work as much as possible.
- For me, figuring out how to code a boring or repetitive task is much more interesting than actually doing the task.
- My code provides documentation as to exactly what I did.
- It's fun. To me, figuring out how to make a computer do something is like a game (with exceptions, of course!).

## What is Python?

The [Python](https://www.python.org/) language was created in the late 80's and named after the BBC television show Monty Python's Flying Circus (later shortened to Monty Python).

![Monty Python](images/python.gif)

Python is not specific to GIS or Esri, and has in fact been around a lot longer than the ArcGIS that you're familiar with. In recent years it has become extremely popular for both GIS and data analysis, but it's a full-fledged programming language that can be used for pretty much anything.

Python is relatively easy to learn, in part because its syntax is more like English than some other languages. It uses whitespace to structure the code, so you don't need to remember nearly as many punctuation rules as you do for some other languages. It also takes care of a lot of details for you, such as memory management.

Python is an interpreted language, which means that your code doesn't need to be compiled into an executable program before you can run it. So instead of creating an .exe file that will only run on Windows, most Python code can run on any operating system that has a Python interpreter available. That means that if you don't use something in your program that is specific to Windows, then you can run the same program on a Mac with no changes. (Unfortunately, ArcGIS only runs on Windows, so your ArcGIS scripts are still only useable on Windows. Sorry.) The fact that Python is interpreted means that you can use it interactively, which also helps *a ton* when learning it!

If you're an ArcMap user, you'll be especially interested to know that there are two main Python branches. Version 2.7 is the last of the 2.x branch and has mostly been replaced by the 3.x branch (which is already up to 3.9). The biggest reason I even mention this is because ArcMap only works with version 2.7, and each version of ArcGIS Pro only works with a specific version 3.x (they don't all use the same one). So if you plan on using your code with both ArcGIS Pro and ArcMap, you'll have to be careful to write code that will work with either version of Python. Esri has been good about keeping their function calls mostly the same between the two versions, otherwise this wouldn't even be an option.

## Writing Python code

You can write code in any plain text editor, so if you happen to be a geek like me and have strong opinions on text editors, then you can still use whichever one makes you happy. Plain text editors include packages like Visual Studio Code, Sublime Text, or Notepad++. Microsoft Word is ***not*** a plain text editor, so don't try to use it. There are some environments that are designed specifically for Python, such as Spyder and PyCharm, which are helpful when debugging code, but you don't need them to write Python code.

Text editors are the traditional way to write code, but right now  [Jupyter](https://jupyter.org/) Notebooks are an incredibly popular alternative for some use cases. Notebooks allow you to mix text, code, and output into a single document, and you can convert them to other file types. I almost always use notebooks when I'm exploring data or trying to figure out how to do something, which is why I'm having you use them for your assignments. And starting with version 2.5, you can use Jupyter Notebooks *inside* of ArcGIS Pro, although the implementation is not without its bugs. 

So with that, let's see how to use Jupyter Notebooks...

## Starting Jupyter if you didn't put your class folder inside your Windows profile folder

The Jupyter shortcut created when you installed ArcGIS only allows Jupyter to open files that are in your Windows profile folder, so you **must** follow these six steps if you saved your class folder somewhere else. They're optional for everyone else, but you can follow them if you don't want to have to navigate to your class folder every time you open Jupyter.

1. Right-click on the `1-setup-and-python-intro-1\tools\run-jupyter-notebook.bat` file and choose **Edit**.
2. Change the first line of the file so that it points to your class folder (for example, `d:\classes\python` instead of `d:\classes\python\1-setup-and-python-intro-1`).
```
SET folder=full_path_to_YOUR_class_folder
```
3. **DO NOT** change anything else in the file.
4. Save the file.
5. Test the file by double-clicking on it. A new browser window like the one in the screenshot below should open. If not, something is wrong. If you're sure you set the first line correctly, contact me for help.
6. Copy the file to a more convenient location, like your desktop. **You'll use this file to open Jupyter instead of the shortcut in your Start Menu.**

## Starting Jupyter if you used your Windows profile folder

Open the Windows **Start** menu, find the **ArcGIS** group, and click on the **Jupyter Notebook** entry. If you have previously installed Anaconda or another Python distribution that includes Jupyter, make sure you use the shortcuts in the **ArcGIS** group. If you use a different Jupyter Notebook shortcut then you won't be able to use ArcGIS in your Python code.

If you want Jupyter to open directly to your class folder instead of your profile folder, follow the directions above for starting Jupyter without using your profile folder.

## For everybody, no matter how you start Jupyter

When you start Jupyter, you'll see a Command Prompt window open, and then your web browser will open. You can minimize the Command Prompt window, but don't close it. If you close it, you'll kill Jupyter. Jupyter actually starts up a little web server on your computer, and you can watch what it's doing in this window if you're really interested.

The web browser will open to a window that looks like this:

![Jupyter](images/jupyter_home.png)

Starting with ArcGIS 2.7 (I think), you'll have the Nbextensions link at the top of the page. If you have it, go ahead and click on it to see what optional extensions you can turn on. One **really useful** one is *Table of Contents (2)*, which will add a table of contents to your notebooks. This is really nice for the larger notebooks. You might also want the *highlighter* so you can highlight important text in notebooks. The *Variable Inspector* is really useful, but notebooks don't work right inside ArcGIS if you turn this one on (don't ask me how long it took to figure out why my notebooks stopped working inside ArcGIS after installing 2.7 and it tried to load my default extensions from other Jupyter installs). When you're done with the extensions page, click on the Files link to go back to the list of files.

Click on a directory name to drill down into it, and navigate to your assignment folder that was created with GitHub Desktop. Click on the `2-test-setup.ipynb` file. That'll open a new browser tab with your first Jupyter Notebook. Read and follow the directions contained in it.
