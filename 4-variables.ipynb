{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Variables\n",
    "\n",
    "A basic part of programming is using variables. If you managed to pass algebra, you're already familiar with this concept, but now you're going to set `x` to some value instead of solve for it. In algebra, `x` can be any number. In programming, `x` can be a number, a letter, a string, a list of things, or something else altogether. The point is that you can reference whatever is stored in x by simply using `x`. You can also change the value stored in `x`.\n",
    "\n",
    "You can assign a value to a variable name using the equals sign. Just like in algebra, the left-hand side is the variable name and the right-hand side is the value. So this example creates a variable called `transect_length`, and it stores a value of `6`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "transect_length = 6"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use the variable name to access its value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "transect_length"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you've set a variable the value won't change unless you explicitly set it to a new value. Here, you're using the variable to calculate a new value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "transect_length * 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But see, the value of the variable didn't change:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "transect_length"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This example will change the stored value because you're assigning it again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "transect_length = transect_length * 2\n",
    "transect_length"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How does that work without getting confused about which value of `transect_length` to use? Well, whatever is on the right-hand side of the equal sign is calculated first, and then the result is assigned to the variable on the left side. So the *current* value of `transect_length` (6) is used for the calculation, and once that's done, *then* the value of `transect_length` is overwritten with the result (12)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The value of one variable can also be assigned to another variable. Now both `transect_distance` and `transect_length` hold the same value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "transect_distance = transect_length\n",
    "print(transect_distance, transect_length)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*This has nothing to do with variables, but notice that I used `print()` that time. Get rid of the `print()`, so the last line just looks like this:*\n",
    "\n",
    "```python\n",
    "transect_distance, transect_length\n",
    "``` \n",
    "\n",
    "*and see how it displays the result.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you set two variables to the same value, and it's something simple like a number or string, you can change the original variable and the second one won't change, and vice versa. In this case, the value of `transect_length` doesn't change when `transect_distance` does."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "transect_distance = 15\n",
    "print('transect_distance is now', transect_distance)\n",
    "print('transect_length is still', transect_length)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also use multiple variables in a calculation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 7\n",
    "y = 2\n",
    "total = x + y\n",
    "total"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Changing the value assigned to a variable doesn't change anything that has already happened.\n",
    "\n",
    "Only things that happen after the change are affected. In this example, the value of `total` doesn't change when the value of `x` changes. It's still equal to 9, even though now `x` is 10 so `x + y` is 12."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 10\n",
    "total"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, you could update the value of `total` using the new value of `x` if you needed to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "total = x + y\n",
    "total"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Assigning multiple variables at a time\n",
    "\n",
    "You can also assign more than one variable at once."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, y = 10, 15\n",
    "print('x', x)\n",
    "print('y', y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you assign multiple values at once, there needs to be the same number of values as there are variables being assigned to. Neither of the next two examples will work because the number of values doesn't match the number of variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, y = 10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An `iterable` is something that can be iterated or looped over, like a list of items. Here Python wants an iterator so that it can break it up and put a part of it in each of the two variables, `x` and `y`. An integer (e.g. 10) can't be broken up that way, though, which is why you got an error that says that `'int' object is not iterable`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, y = 10, 15, 20"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, `too many values to unpack` means that it's trying to unpack, or separate, the *three* values on the right-hand side of the equal sign, and assign them to the variables on the left-hand side, but there are too many of them because there are only *two* variables (`x` and `y`).\n",
    "\n",
    "The one exception to this rule is if you try to assign multiple values to *only one* variable.\n",
    "\n",
    "In this case, Python creates a *tuple* of values and assigns that to the variable (you'll learn more about lists and tuples later, but essentially it's a list that contains all of the values)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 10, 15\n",
    "x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unlike some programming languages, Python doesn't care what type of data you store in a variable. In fact, you can change the type and Python won't care."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('old value of y:', y)\n",
    "y = 'hello world!'\n",
    "print('new value of y:', y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Give your variables descriptive names!\n",
    "\n",
    "No:\n",
    "```python\n",
    "n = 'Cache'\n",
    "``` \n",
    "\n",
    "Yes:\n",
    "```python\n",
    "county = 'Cache'\n",
    "``` \n",
    "\n",
    "You might think you know what data a variable called `'n'` holds right now, but you won't remember if you need to use your code a month from now. If you give your code to someone else, they won't know what it is, either. So if it's the name of a county, call it `county` or `county_name`.\n",
    "\n",
    "> Plus, I'll get after you if you don't name your variables appropriately, and I'd rather not have to do that."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "# Problem 1\n",
    "(3 parts)\n",
    "\n",
    "**1A.** Create two variables, called `width` and `length`, and use them to store the dimensions of a plot that's 200 meters wide by 1500 meters long. You don't need to worry about the fact that they're meters, though. Store them as numbers because you're going to do some math with them in a minute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "**1B.** Now use your variables to calculate the area of the plot in square meters and store the result in a variable called `area` and print the result. \n",
    "\n",
    "> Use the variables. Do not put any numbers in this bit of code!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "**1C.** Now use the information below to convert the area to hectares and store the result in a variable called `hectares` and print the result.\n",
    "\n",
    "**1 meter<sup>2</sup> = 0.0001 hectares**\n",
    "\n",
    "> Again, the only number in this code will be the conversion factor between square meters and hectares.\n",
    ">\n",
    "> A hectare is larger than a square meter, so your area in hectares (part C) should be a smaller number than your area in square meters (part B). If not, you did your math wrong."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Errors revisited\n",
    "\n",
    "Let's return to errors for a bit. A common mistake is to accidentally reference a variable that doesn't exist yet or is misspelled. For example,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "body_mass_kg = 25\n",
    "body_mass_g = bodymass_kg * 1000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last line of the error message tells you that you tried to use a variable named `bodymass_kg` that doesn't actually exist (i.e. is not defined). The `---->` in the error message points to the line of code that caused the error. If you look closely at the code, you'll see that the line with the error does indeed use a variable called `bodymass_kg`, which is supposed to be the variable defined on the previous line of code. However, the variable from the previous line is called `body_mass_kg`, with an underscore between `body` and `mass`.\n",
    "\n",
    "So the reason that `bodymass_kg` is not defined is that there is a typo in the name, and it should be `body_mass_kg`.\n",
    "\n",
    "**Remember: Error messages are your friend!**\n",
    "\n",
    "> A common mistake is to see an error and try to figure out what's wrong with the code without carefully reading the error message. You'll be able to fix mistakes **much** faster if you learn to understand error messages and pay attention to what they're telling you."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 2\n",
    "\n",
    "Edit the `bodymass_kg` code above so that it runs without an error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 3\n",
    "\n",
    "Use the error message to figure out what the value of `b` is and put your answer in the next cell.\n",
    "\n",
    "```\n",
    "In [9]: print a / b\n",
    "---------------------------------------------------------------------------\n",
    "ZeroDivisionError                         Traceback (most recent call last)\n",
    "<ipython-input-9-9526fd94e1a8> in <module>()\n",
    "----> 1 print a / b\n",
    "\n",
    "ZeroDivisionError: integer division or modulo by zero\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "b = "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Make sure you save your notebook!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now continue to `5-finish.html` for directions to turn in your homework."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "position": {
    "height": "368.212px",
    "left": "1736.67px",
    "right": "20px",
    "top": "120px",
    "width": "350px"
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
