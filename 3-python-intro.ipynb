{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Python\n",
    "\n",
    "This series of short tutorials will introduce you to some basic Python concepts. There are lots of online resources for more information, and a few of them are listed on the More Resources page in Canvas.\n",
    "\n",
    "You need to work through the notebook tutorials and run each code cell so you can see what it does. You can edit any of the code examples and rerun them to see what happens, and I strongly encourage this. If you don't quite understand a concept, play with the code until you do. When you turn this in, I'll be checking to make sure that you've run everything (it's a free point towards your grade!).\n",
    "\n",
    "There are a few homework problems included in most of the notebooks. You'll be given instructions and a code cell to put your answer in.\n",
    "\n",
    "You can navigate through the notebook with the up and down arrow keys or with your mouse.\n",
    "\n",
    "Make sure you save your notebook periodically so you don't lose any work (there's a save button in the toolbar)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Printing data\n",
    "\n",
    "Jupyter Notebooks like this one and interactive Python consoles will automatically print some things out for you. For example, if you run the next cell it just echoes what's in the code and does nothing else. \n",
    "\n",
    "To run the cell, make sure it's selected (it'll have an outline around it) and then either hit <kbd>Shift</kbd>+<kbd>Enter</kbd> or use the **Run** button on the toolbar."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If there are multiple lines of code, it will only echo the **last** one. That's why the next example only prints the 10 and not the 5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "5\n",
    "10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to print out everything, you have to explicitly tell it to print. Just so you know, this is an important difference between Python 2.7 and Python 3.x. You don't need the parentheses if you're using Python 2.7 (like with ArcMap), but do need them for Python 3 (which is what ArcGIS Pro uses)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(5)\n",
    "print(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also print multiple things on a line by separating them with a comma."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(5, 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also print text by surrounding it in quotes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Hello world!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comments\n",
    "\n",
    "It's always a good idea to include comments in your code. Comments are lines of text in the code that are ignored by the Python interpreter. These are generally used to provide documentation about what's going on, but they can also be used to temporarily keep certain lines of code from executing. Python comments start with a hash sign (`#`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The first two lines are comments and will be ignored by Python\n",
    "# print('This is a comment line')\n",
    "print('This is not a comment line')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remove the hash mark from the first line in the previous cell and run it again. It'll fail, because that first line isn't valid Python code. Add the hash mark back in and then the cell will work again.\n",
    "\n",
    "You might not think comments are important while you're working on something, but if you need to run the code a month or two later, you might wish they were there. Also, anyone you give your code to will also appreciate them.\n",
    "\n",
    "Nobody likes writing code documentation (at least nobody I know!), and I'll admit that I get lazy and don't do it sometimes. But if I ever use that code again (which I do a lot), I always end up regretting my laziness. \n",
    "\n",
    "> You should get in the habit of documenting your code from the very beginning. In fact, you'll lose points on your homework if you don't!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Whitespace\n",
    "\n",
    "Python uses whitespace to group things together (more on that later), but because of this, using the correct indentation is absolutely essential. Sometimes it'll cause an error if you mess it up, but other times you'll get the wrong results instead. Here's a simple example of it causing an error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "print(5)\n",
    " print(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, an extra space at the beginning of the line will break things, but extra spaces at the *end* of a line don't matter.\n",
    "\n",
    "**Be sure not to mix tabs and spaces when indenting code.** If you do, you might have two lines of code that look like they're indented the same to you, but one uses a tab and the other uses spaces and Python will see the tab as one character and the spaces as multiple characters and won't like it. The notebook automatically converts tabs to spaces for you, but if you're writing code in another environment you'll definitely run into that problem if you do mix the two."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Errors\n",
    "\n",
    "**Learning to read error messages will save you a lot of heartache, pain, and time!**\n",
    "\n",
    "Let's take a look at that error message you just saw. We'll start with the last line and work our way up. The last line of an error message always tells you the type of error that occurred.\n",
    "\n",
    "```\n",
    "IndentationError: unexpected indent\n",
    "```\n",
    "\n",
    "From that, you know that there was an indentation in a location there shouldn't be. \n",
    "\n",
    "Now look at the next line up in the error. It shows you the line of code that the error occurred on.\n",
    "\n",
    "```\n",
    "    print(10)\n",
    "    ^\n",
    "```\n",
    "\n",
    "Notice the little arrow pointing to the 'p'. That points to the location in the line where the Python parser realized there was a problem. From this you know that there is an extra indentation at the 'p' (meaning right before it). This type of error is called a *syntax error* because the code is formatted wrong and the Python interpreter can't understand it. As you'll see later, spaces mean things in Python, but this space isn't in a location where spaces are allowed.\n",
    "\n",
    "Now go up one more line in the error message, which happens to be the first line in this case.\n",
    "\n",
    "```\n",
    "  File \"<ipython-input-7-479f604e47d4>\", line 2\n",
    "```\n",
    "\n",
    "This tells you which file the error occurred in (which doesn't make a lot of sense in this case, since it's in the notebook and it doesn't show an actual filename), and also which line of the file the error occurred on. Sure enough, that extra space before the 'p' is on line 2 of the code cell."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 1\n",
    "\n",
    "Now you have the information you need to fix the error-- just delete that extra space before the \"p\" so that there is no extra indentation. **Edit the previous code cell and run it to make sure the error is fixed.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# True and False (Booleans)\n",
    "\n",
    "`True` and `False` are Python keywords that mean exactly what you think they mean. They're case-sensitive, as you can see here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(true)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's not lying to you-- `true` really isn't defined (meaning Python doesn't know what it is). It needs a capital T."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use the standard Boolean operations of `and`, `or`, and  `not`. For example, `and` means that everything must be `True` in order for the result to be `True`. Notice that each line of this cell is printing two things: \n",
    "1. A description \n",
    "2. The result of a Boolean operation (e.g. `True and True`) that is done on-the-fly"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('True and True is', True and True)\n",
    "print('True and False is', True and False)\n",
    "print('False and False is', False and False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`or` means that only one of the inputs needs to be `True` in order for the result to be `True`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('True or True is', True or True)\n",
    "print('True or False is', True or False)\n",
    "print('False or False is', False or False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`not` negates something. So `True` becomes `False`, and vice versa."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('not True is', not True)\n",
    "print('not False is', not False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Those examples are just to show you how the operators work, but it's more common to use statements that evaluate to `True` or `False`, rather than using the words `True` and `False`. You'll see how that works later, but for now just see that statements can evaluate to `True` or `False`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "5 < 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "5 < 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "5 < 10 and 5 < 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "5 < 10 or 5 < 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You could use parentheses in those last two examples if it makes it more clear what's happening, but it's not necessary. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(5 < 10) or (5 < 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Make sure you save your notebook!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now go back to the main Jupyter Notebook browser tab and open the `4-variables.ipynb` notebook and work through it."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
